import common
import re

def FullOTA_Assertions(info):
  # If we are shipping firmware, make sure that we only flash on supported variants
  if "RADIO/filemap" in info.input_zip.namelist() in info.input_zip.namelist():
    AddBlModelAssertion(info)

def FullOTA_InstallEnd(info):
  skip_firmware = False

  # Skip Firmware if filemap or any of the images are not present
  if "RADIO/filemap" not in info.input_zip.namelist():
    skip_firmware = True
  else:
    filemap = info.input_zip.read("RADIO/filemap").decode('utf-8').splitlines()
    for file in filemap:
      filename = file.split(" ")[0]
      if "RADIO/{}".format(filename) not in info.input_zip.namelist():
        skip_firmware = True
        break

  if "IMAGES/recovery.img" in info.input_zip.namelist():
    info.script.AppendExtra('ui_print("Patching recovery image unconditionally...");')
    info.script.AppendExtra('package_extract_file("recovery.img", "/dev/block/platform/11120000.ufs/by-name/RECOVERY");')

  if not skip_firmware:
    CopyFirmware(info.input_zip, info.output_zip)
    AddFirmwareUpdate(info, filemap)

def CopyFirmware(input_zip, output_zip):
  for info in input_zip.infolist():
    f = info.filename
    # Copy files in 'RADIO' to output zip 'firmware-update'
    if f.startswith("RADIO/") and (f.__len__() > len("RADIO/")):
      fn = f[6:]
      common.ZipWriteStr(output_zip, "firmware-update/" + fn, input_zip.read(f))

def AddBlModelAssertion(info):
  info.script.AppendExtra('ifelse(universal9810.verify_bootloader_model() != "1",')
  info.script.AppendExtra('ui_print("=============================================");');
  info.script.AppendExtra('ui_print("=============================================");');
  info.script.AppendExtra('ui_print("                   ERROR:                    ");');
  info.script.AppendExtra('ui_print("         Package contains firmware!          ");');
  info.script.AppendExtra('ui_print("         Only the following models           ");');
  info.script.AppendExtra('ui_print("             are supported: F, N             ");');
  info.script.AppendExtra('ui_print("         Cross flashing will brick!          ");');
  info.script.AppendExtra('ui_print("=============================================");');
  info.script.AppendExtra('ui_print("============NO CHANGE HAS BEEN MADE==========");');
  info.script.AppendExtra('abort(" ");')
  info.script.AppendExtra(');')

def CheckVariant(info, filemap, target_variant):
  # check if firmware for the variant is present
  if "RADIO/" + target_variant + "_sboot.bin" in info.input_zip.namelist():
   info.script.AppendExtra('ifelse(universal9810.compare_variant("' + target_variant + '") == "1",')
   info.script.AppendExtra('(')
   for file in filemap:
     filename = file.split(" ")[0]
     filepath = file.split(" ")[-1]
     filevariant = filename.split("_")[0]
     if filevariant == target_variant:
       info.script.AppendExtra('package_extract_file("firmware-update/' + filename + '", "' + filepath + '");')
   info.script.AppendExtra(')')
   info.script.AppendExtra(');')

def AddFirmwareUpdate(info, filemap):
  info.script.AppendExtra('ifelse(universal9810.verify_bootloader_min() == "1",')
  info.script.AppendExtra('(')
  info.script.AppendExtra('ui_print("Upgrading firmware");')

  # Supported models are the same for S9, S9+ and Note 9
  CheckVariant(info, filemap, "F")
  CheckVariant(info, filemap, "N")

  info.script.AppendExtra('run_program("/sbin/reboot","fota_bl");')
  info.script.AppendExtra('),')
  info.script.AppendExtra('(')
  info.script.AppendExtra('ui_print("Firmware is up-to-date");')
  info.script.AppendExtra(')')
  info.script.AppendExtra(');')

